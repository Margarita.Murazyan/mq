package com.mentor_click.mq.model.view;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@Builder
public class EmployeesMatchesCase {
    List<EmployeesView> cases;


}
