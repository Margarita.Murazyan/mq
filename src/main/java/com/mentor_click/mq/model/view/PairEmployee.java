package com.mentor_click.mq.model.view;


import com.mentor_click.mq.model.Employee;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class PairEmployee {

    private Employee firstEmployee;
    private Employee secondEmployee;
    private int percentage;
}
