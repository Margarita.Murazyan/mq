package com.mentor_click.mq.model.view;

import com.mentor_click.mq.model.view.PairEmployee;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@ToString
public class EmployeesView {
    private List<PairEmployee> pairEmployee;
    private double average;

    /*public EmployeesView(EmployeesView employeesView){
        this.pairEmployee = employeesView.pairEmployee;
        this.average = employeesView.average;
    }*/
}
