package com.mentor_click.mq.model;

import com.opencsv.bean.CsvBindByName;
import lombok.*;

import java.util.Objects;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
//@EqualsAndHashCode(of = {"email"})
public class Employee {

    @CsvBindByName(column = "Name")
    private String name;

    @CsvBindByName(column = "Email")
    private String email;

    @CsvBindByName(column = "Division")
    private String division;

    @CsvBindByName(column = "Age")
    private int age;

    @CsvBindByName(column = "Timezone")
    private int timeZone;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        Employee employee = (Employee) o;
        return getEmail().equals(employee.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmail());
    }
}
