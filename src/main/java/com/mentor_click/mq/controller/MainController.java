package com.mentor_click.mq.controller;

import com.mentor_click.mq.exception.CVSFileException;
import com.mentor_click.mq.model.Employee;
import com.mentor_click.mq.model.view.EmployeesMatchesCase;
import com.mentor_click.mq.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller()
@Slf4j
public class MainController {

    private EmployeeService employeeService;

    @Autowired
    public MainController(EmployeeService employeeService){
        this.employeeService = employeeService;
    }


    /**
     * Returns the main page where we can enter the path to the CSV file
     * and get perfect matches for the employee data
     * */
    @GetMapping
    public String getMainPage(){
        return "index";
    }

    @GetMapping("/best-matches")
    public String greetingSubmit(@RequestParam(value = "csvFilePath", required = false) String csvFilePath,
                                 Model model) throws CVSFileException {
        log.info("Load cvs file: path= "+csvFilePath);
        List<Employee> employees = employeeService.loadEmployeeFromCSVFile(csvFilePath);
        EmployeesMatchesCase dataOfMatchingPairs = employeeService.getDataOfMatchingPairs(employees);
        model.addAttribute("data", dataOfMatchingPairs);
        model.addAttribute("error", null);
        return "index";
    }



    @ExceptionHandler(CVSFileException.class)
    public ModelAndView handleException(CVSFileException ex)
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.getModelMap().addAttribute("error", ex.getMessage());
        return modelAndView;
    }
}
