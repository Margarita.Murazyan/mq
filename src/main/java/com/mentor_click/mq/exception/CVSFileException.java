package com.mentor_click.mq.exception;

public class CVSFileException extends Exception{

    public CVSFileException(String errorMessage, Throwable err){
        super(errorMessage, err);
    }
}
