package com.mentor_click.mq.service;

import com.mentor_click.mq.exception.CVSFileException;
import com.mentor_click.mq.model.Employee;
import com.mentor_click.mq.model.view.EmployeesMatchesCase;

import java.util.List;

public interface EmployeeService {


    /**
     * Load employees from csv file
     *@param filePath specified path of CSV file
     * return list of {@link Employee}
     * */
    List<Employee> loadEmployeeFromCSVFile(String filePath) throws CVSFileException;


    /**
     *  Present the ideal matches for the given employees.
     *
     *  this method calls the private method `compute`
     * @param employees
     * return {@link EmployeesMatchesCase}
     * */
    EmployeesMatchesCase getDataOfMatchingPairs(List<Employee> employees);
}
