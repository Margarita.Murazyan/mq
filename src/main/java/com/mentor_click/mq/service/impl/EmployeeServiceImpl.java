package com.mentor_click.mq.service.impl;

import com.mentor_click.mq.exception.CVSFileException;
import com.mentor_click.mq.model.Employee;
import com.mentor_click.mq.model.view.EmployeesMatchesCase;
import com.mentor_click.mq.model.view.EmployeesView;
import com.mentor_click.mq.model.view.PairEmployee;
import com.mentor_click.mq.service.EmployeeService;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.util.*;

@Service
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

    private final int AGE_COEFFICIENT = 30;
    private final int DIVISION_COEFFICIENT = 30;
    private final int TIME_ZONE_COEFFICIENT = 40;

    @Override
    public List<Employee> loadEmployeeFromCSVFile(String filePath) throws CVSFileException {
        List<Employee> employees = new ArrayList<>();
        try {
            employees = new CsvToBeanBuilder(new FileReader(filePath))
                    .withType(Employee.class)
                    .withSeparator(',')
                    .build()
                    .parse();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new CVSFileException(e.getMessage(), e);
        }
        return employees;
    }

    @Override
    public EmployeesMatchesCase getDataOfMatchingPairs(List<Employee> employees) {
        EmployeesMatchesCase results = EmployeesMatchesCase.builder()
                .cases(new ArrayList<>()).build();
        compute(employees, EmployeesView.builder()
                        .pairEmployee(new ArrayList<>())
                        .build(), results);
        return results;
    }

    private  void compute(List<Employee> set,
                                EmployeesView currentResults,
                                EmployeesMatchesCase results) {
        if (set.size() < 2) {
                results.getCases().add(EmployeesView.builder()
                        .pairEmployee(new ArrayList<>(currentResults.getPairEmployee()))
                        .average(currentResults.getAverage())
                        .build());
            return;
        }
        List<Employee> list = new ArrayList<Employee>(set);
        Employee first = list.remove(0);
        for (int i = 0; i < list.size(); i++) {
            Employee second = list.get(i);
            List<Employee> nextSet = new LinkedList<>(list);
            nextSet.remove(second);
            int percentage = Math.abs(first.getAge() - second.getAge()) <= 5 ? AGE_COEFFICIENT : 0;
            percentage += second.getDivision().equals(first.getDivision()) ? DIVISION_COEFFICIENT : 0;
            percentage += first.getTimeZone()==second.getTimeZone() ? TIME_ZONE_COEFFICIENT :0;
            PairEmployee pair = PairEmployee.builder()
                    .percentage(percentage)
                    .firstEmployee(first)
                    .secondEmployee(second)
                    .build();
            currentResults.getPairEmployee().add(pair);
            currentResults.setAverage(currentResults.getPairEmployee().stream().mapToInt(
                    PairEmployee::getPercentage).average().orElse(0));
            compute(nextSet, currentResults, results);
            currentResults.getPairEmployee().remove(pair);
        }
    }

/*
    private  void compute(Set<Employee> set,
                                List<List<Employee>> currentResults,
                                List<List<List<Employee>>> results) {
        if (set.size() < 2) {
            results.add(new ArrayList<List<Employee>>(currentResults));
            return;
        }
        List<Employee> list = new ArrayList<Employee>(set);
        Employee first = list.remove(0);
        for (int i = 0; i < list.size(); i++) {
            Employee second = list.get(i);
            Set<Employee> nextSet = new LinkedHashSet<Employee>(list);
            nextSet.remove(second);

            List<Employee> pair = Arrays.asList(first, second);
            currentResults.add(pair);
            compute(nextSet, currentResults, results);
            currentResults.remove(pair);
        }
    }*/
}
