package com.mentor_click.mq.service;

import com.mentor_click.mq.model.Employee;
import com.mentor_click.mq.model.view.EmployeesMatchesCase;
import com.mentor_click.mq.model.view.EmployeesView;
import com.mentor_click.mq.model.view.PairEmployee;
import com.mentor_click.mq.service.impl.EmployeeServiceImpl;
import lombok.SneakyThrows;

import java.util.List;

public class Test {

    @SneakyThrows
    public static void main(String[] args) {
        EmployeeServiceImpl employeeService = new EmployeeServiceImpl();
        List<Employee> employees = employeeService.loadEmployeeFromCSVFile("C:\\Users\\User\\Desktop\\employee1.csv");
        for (EmployeesView employeesView :  employeeService.getDataOfMatchingPairs(employees).getCases()) {
            for (PairEmployee pairEmployee : employeesView.getPairEmployee()) {
                System.out.println(pairEmployee.getFirstEmployee().getName() + " with " + pairEmployee.getSecondEmployee().getName() +" = "+pairEmployee.getPercentage()+ " |");
            }
            System.out.println("Average score = "+employeesView.getAverage());
            System.out.println();
        }
    }
}
