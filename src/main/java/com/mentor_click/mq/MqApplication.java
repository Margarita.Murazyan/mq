package com.mentor_click.mq;

import com.mentor_click.mq.model.Employee;
import com.mentor_click.mq.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class MqApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(MqApplication.class, args);
    }

    @Autowired
    private EmployeeService employeeService;

    @Override
    public void run(String... args) throws Exception {
        String cvsFilePath = "C:\\Users\\User\\Desktop\\employee.csv";
        List<Employee> employees = employeeService.loadEmployeeFromCSVFile(cvsFilePath);
        for (Employee employee : employees) {
            System.out.println("Employee "+employee);
        }
    }
}
